# REQUIRED PARAMETERS

variable "records" {
  description = "Lista de maps de los Recors DNS"
  type        = any
}


# OPTIONAL PARAMETERS

variable "create" {
  description = "Indica si se crean los records o no"
  type        = bool
  default     = true
}

variable "zone_id" {
  description = "ID de la zona DNS. Obligatorio, a menos que se mande zone_name"
  type        = string
  default     = null
  
}

variable "zone_name" {
  description = "Nombre de la zona DNS. Obligatorio, a menos que se mande zone_id"
  type        = string
  default     = null

}

variable "private_zone" {
  description = "Indica si la zona es privada o no"
  type        = bool
  default     = false
}