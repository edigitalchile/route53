output "this_route53_record_name" {
  description = "Nombre del record"
  value       = { for k, v in aws_route53_record.this : k => v.name }
}

output "this_route53_record_fqdn" {
  description = "FQDN construido usando la zona de dominio y el nombre"
  value       = { for k, v in aws_route53_record.this : k => v.fqdn }
}