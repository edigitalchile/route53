# Modulo de terraform para crear records en zonas de route53

### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| records  | Lista de maps de los Recors DNS  | SI  | N/A  |
| zone_id  | ID de la zona DNS. Obligatorio, a menos que se mande zone_name  | NO  | null  |
| zone_name  | Nombre de la zona DNS. Obligatorio, a menos que se mande zone_id  | NO  | null  |
| create  | Indica si se crean los records o no  | NO  | true  |
| private_zone  | Indica si la zona es privada o no  | NO  | false |



### Outputs

| Nombre  | Descripción  |
|---|---|
| this_route53_record_name  | Nombre del record  |
| this_route53_record_fqdn  | FQDN construido usando la zona de dominio y el nombre  |